module.exports = {
  exclude: [
    "**/node_modules/**/*",
    "**/.git/**/*"
  ],
  plugins: [
    ["snowpack-flow-remove-types"]
  ],
  optimize: {
    bundle: true,
    minify: true,
    target: "es6"
  },
  buildOptions: {
    sourcemap: "inline",
    out: "public"
  }
};
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsiL1VzZXJzL0Jlbi93b3Jrc3BhY2UvZW1vamljbGlja2VyL3Nub3dwYWNrLmNvbmZpZy5qcyJdLAogICJtYXBwaW5ncyI6ICJBQUlBLE9BQU8sVUFBVTtBQUFBLEVBQ2YsU0FBUztBQUFBLElBQ1A7QUFBQSxJQUNBO0FBQUE7QUFBQSxFQUVGLFNBQVM7QUFBQSxJQUNQLENBQUM7QUFBQTtBQUFBLEVBRUgsVUFBVTtBQUFBLElBQ1IsUUFBUTtBQUFBLElBQ1IsUUFBUTtBQUFBLElBQ1IsUUFBUTtBQUFBO0FBQUEsRUFFVixjQUFjO0FBQUEsSUFDWixXQUFXO0FBQUEsSUFDWCxLQUFLO0FBQUE7QUFBQTsiLAogICJuYW1lcyI6IFtdCn0K
