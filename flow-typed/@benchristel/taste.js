// @flow

declare module "@benchristel/taste" {
    declare type Predicate = (...args: Array<any>) => boolean
    declare opaque type Tests
    declare opaque type TestResults

    declare export function test(
        title: string,
        scenarios: {[string]: () => mixed},
    ): mixed

    declare export function expect(
        actual: mixed,
        Predicate,
        ...expected: Array<mixed>
    ): mixed

    declare export function is(mixed, mixed): boolean
    declare export function equals(mixed, mixed): boolean

    declare export function getAllTests(): Tests
    declare export function runTests(Tests): Promise<TestResults>
    declare export function formatTestResultsAsText(TestResults): string
}